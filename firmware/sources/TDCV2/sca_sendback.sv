/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : sca_sendback.v
// Create : 2022-08-29 17:56:29
// Revise : 2023-10-11 03:51:00
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------
  
module sca_sendback
    #(parameter DOWNLINKCOUNT = 1,
      parameter SCA_PER_DOWNLINK = 4)
    (
    input           clk,
    input           rst,

    input   [SCA_PER_DOWNLINK-1:0] rx_reply_received_i[DOWNLINKCOUNT-1:0],
    input   [7:0]   rx_address_i[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0],
    input   [7:0]   rx_transID_i[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0],
    input   [7:0]   rx_channel_i[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0],
    input   [7:0]   rx_len_i    [DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0],
    input   [7:0]   rx_error_i  [DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0],
    input   [31:0]  rx_data_i   [DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0],

    input           uart_tx_ready_i,
    output  [7:0]   uart_data_o,
    output          uart_data_write_o

);
    

wire any_busy_rising;
wire any_busy_falling;
wire [7:0]   uart_data_array[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];
wire [7:0]   uart_data_array_tmp[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];
wire [SCA_PER_DOWNLINK-1:0] uart_data_write_array[DOWNLINKCOUNT-1:0];
wire [SCA_PER_DOWNLINK*DOWNLINKCOUNT-1:0] uart_data_write_array_tmp;
wire [SCA_PER_DOWNLINK*DOWNLINKCOUNT-1:0] sca_busy,sca_send_enable_array;
reg  [SCA_PER_DOWNLINK*DOWNLINKCOUNT-1:0] sca_busy_reg, sca_busy_sample;
wire [SCA_PER_DOWNLINK*DOWNLINKCOUNT-1:0] uart_data_array_8[7:0];

assign any_busy_rising = |(sca_busy & (~sca_busy_reg));
assign any_busy_falling = |((~sca_busy) & sca_busy_reg);

multi_hot_one_hot #(.WIDTH(SCA_PER_DOWNLINK*DOWNLINKCOUNT)) 
inst_multi_hot_one_hot (.multi_hot_code(sca_busy_sample), .one_hot_code(sca_send_enable_array));


always @(posedge clk  ) begin
    sca_busy_reg <= sca_busy;
end

always @(posedge clk  ) begin
    if (rst) begin
        // reset
        sca_busy_sample <= 'b0;
    end else if ((sca_busy_sample=='b0)&any_busy_rising) begin
        sca_busy_sample <= sca_busy;
    end else if (any_busy_falling) begin
        sca_busy_sample <= sca_busy;
    end
end                       

genvar i,j,k,m,n;
generate
    for (i=0;i<DOWNLINKCOUNT;i=i+1) begin 
        for (j=0;j<SCA_PER_DOWNLINK;j=j+1) begin 
            sca_sendback_single inst_sca_sendback_single(
                .clk                 (clk),
                .rst                 (rst),
                .send_enable_i       (sca_send_enable_array[i*SCA_PER_DOWNLINK+j]),
                .rx_reply_received_i (rx_reply_received_i[i][j]),
                .rx_address_i        (rx_address_i[i][j]),
                .rx_transID_i        (rx_transID_i[i][j]),
                .rx_channel_i        (rx_channel_i[i][j]),
                .rx_len_i            (rx_len_i[i][j]),
                .rx_error_i          (rx_error_i[i][j]),
                .rx_data_i           (rx_data_i[i][j]),
                .uart_tx_ready_i     (uart_tx_ready_i),
                .uart_data_o         (uart_data_array[i][j]),
                .uart_data_write_o   (uart_data_write_array[i][j]),
                .sca_busy_o          (sca_busy[i*SCA_PER_DOWNLINK+j])
            );
            assign uart_data_array_tmp[i][j]=uart_data_array[i][j]&{8{sca_send_enable_array[i*SCA_PER_DOWNLINK+j]}};
            assign uart_data_write_array_tmp[i*SCA_PER_DOWNLINK+j]=uart_data_write_array[i][j]&sca_send_enable_array[i*SCA_PER_DOWNLINK+j];
        end
    end
    for (k=0;k<8;k=k+1) begin 
        for (m=0;m<DOWNLINKCOUNT;m=m+1) begin 
            for (n=0;n<SCA_PER_DOWNLINK;n=n+1) begin 
                assign uart_data_array_8[k][m*SCA_PER_DOWNLINK+n] = uart_data_array_tmp[m][n][k];
            end
        end
        assign uart_data_o[k] = |uart_data_array_8[k];
    end
endgenerate

assign uart_data_write_o = |uart_data_write_array_tmp;

endmodule

