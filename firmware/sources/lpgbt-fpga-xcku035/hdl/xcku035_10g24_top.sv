/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : xcku035_10g24_top.sv
// Create : 2023-10-12 23:04:23
// Revise : 2023-10-13 05:24:31
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------


module xcku035_10g24_top(
    // --===============--
    //   -- Clocks scheme --
    //   --===============--       
    //   -- MGT(GTX) reference clock:
    //   ----------------------------
      
    //   -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
    //   --
    //   --          * The MGT reference clock frequency must be 320MHz.
    input SMA_MGT_REFCLK_P,
    input SMA_MGT_REFCLK_N,
        
      // -- Fabric clock: 200MHz
    input USER_CLOCK_P,
    input USER_CLOCK_N,

    input TRIGGER0_P,
    input TRIGGER0_N,

    // --hit to mezz
    output [23:0] hit,
    output mezz_TRST,

    // -- MGT(GTX) --
    output [0:0] SFP_TX_P,
    output [0:0] SFP_TX_N,
    input [0:0] SFP_RX_P,
    input [0:0] SFP_RX_N,
       
    // -- SFP control
    output [7:0] SFP_TX_DISABLE,

    // --====================--
    // -- Signals forwarding --
    // --====================--

    // -- SMA output:
    // --------------
    output USER_SMA_GPIO_P,
    output USER_SMA_GPIO_N,


    // -- 125 MHz clock from MMCM
    // --      gtx_clk_bufg_out:       OUT STD_LOGIC;
    output phy_resetn,

    // -- RGMII Interface
    // ------------------
    output [3:0] rgmii_txd,
    output rgmii_tx_ctl,
    output rgmii_txc,
    input [3:0] rgmii_rxd,
    input rgmii_rx_ctl,
    input rgmii_rxc,

    // -- MDIO Interface
    //     -----------------
    inout mdio,
    output mdc,

    // -- UART Interface
    input uart_rxd,
    output uart_txd
);

    localparam UPLINKCOUNT = 1;
    localparam DOWNLINKCOUNT = 1;
    localparam SCA_PER_DOWNLINK = 4;
    
    localparam FEC5           =1;
    localparam FEC12          =2;
    localparam DATARATE_5G12  =1;
    localparam DATARATE_10G24 =2;
    localparam PCS            =0;
    localparam PMA            =1;

    // -- Clocks:
    wire mgtRefClk_from_smaMgtRefClkbuf;

    wire mgt_freedrpclk;

    wire lpgbtfpga_mgttxclk;
    wire lpgbtfpga_mgtrxclk;


    // -- User CDC for lpGBT-FPGA      
    wire lpgbtfpga_clk40;

    wire [2:0] uplinkPhase[UPLINKCOUNT-1:0];
    wire [2:0] uplinkPhaseCalib[UPLINKCOUNT-1:0];
    wire [UPLINKCOUNT-1:0] uplinkPhaseForce;
                   
                                                                     
    wire [9:0] downlinkPhase[DOWNLINKCOUNT-1:0];
    wire [9:0] downlinkPhaseCalib[DOWNLINKCOUNT-1:0];
    
    wire [DOWNLINKCOUNT-1:0] downlinkPhaseForce;

    // -- LpGBT-FPGA
    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_downlinkrst;
    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_downlinkrdy;
                                 

    wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkrst; 
    wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkrdy;
    

    wire [31:0] lpgbtfpga_downlinkUserData_fromgen [DOWNLINKCOUNT-1:0];
    wire [31:0] lpgbtfpga_downlinkUserData[DOWNLINKCOUNT-1:0];
    

    wire [1:0] lpgbtfpga_downlinkEcData[DOWNLINKCOUNT-1:0];
    wire [1:0] lpgbtfpga_downlinkIcData[DOWNLINKCOUNT-1:0];

    wire [229:0] lpgbtfpga_uplinkUserData[UPLINKCOUNT-1:0];
    wire [1:0] lpgbtfpga_uplinkEcData[UPLINKCOUNT-1:0];
    wire [1:0] lpgbtfpga_uplinkIcData[UPLINKCOUNT-1:0];
    wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkclk;


    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txpolarity;

    wire [UPLINKCOUNT-1:0] lpgbtfpga_mgt_rxpolarity;


    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txaligned;
    wire [6:0]lpgbtfpga_mgt_txpiphase[DOWNLINKCOUNT-1:0];
    wire [6:0]lpgbtfpga_mgt_txpicalib[DOWNLINKCOUNT-1:0];
    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txcaliben;
                    
    wire [DOWNLINKCOUNT-1:0] downLinkBypassInterleaver;
    wire [DOWNLINKCOUNT-1:0] downLinkBypassFECEncoder;
    wire [DOWNLINKCOUNT-1:0] downLinkBypassScrambler;

    wire [UPLINKCOUNT-1:0] upLinkScramblerBypass;
    wire [UPLINKCOUNT-1:0] upLinkFecBypass;
    wire [UPLINKCOUNT-1:0] upLinkInterleaverBypass;

    wire [UPLINKCOUNT-1:0] upLinkFECCorrectedClear;
    wire [UPLINKCOUNT-1:0] upLinkFECCorrectedLatched;



    // -- Config
    wire uplinkSelectDataRate = 1'b0;

    wire [DOWNLINKCOUNT-1:0] generator_rst;
    wire [1:0] downconfig_g0[DOWNLINKCOUNT-1:0];
    wire [1:0] downconfig_g1[DOWNLINKCOUNT-1:0];
    wire [1:0] downconfig_g2[DOWNLINKCOUNT-1:0];
    wire [1:0] downconfig_g3[DOWNLINKCOUNT-1:0];
    wire [15:0] downlink_gen_rdy[DOWNLINKCOUNT-1:0];

    wire [55:0] upelink_config[UPLINKCOUNT-1:0];
    wire [27:0] uperror_detected[UPLINKCOUNT-1:0];
    wire [UPLINKCOUNT-1:0] reset_upchecker;
    // ------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------

    // --SCA debug
    // -----------
    wire [SCA_PER_DOWNLINK-1:0] sca_enable_array[DOWNLINKCOUNT-1:0];
    wire [DOWNLINKCOUNT-1:0]sca_reset;
    wire [DOWNLINKCOUNT-1:0]sca_ready;
    wire [DOWNLINKCOUNT-1:0]sca_start;
    wire [DOWNLINKCOUNT-1:0]sca_start_reset;
    wire [DOWNLINKCOUNT-1:0]sca_start_connect;

    wire [159:0] sca_rx_parr[DOWNLINKCOUNT-1:0];


    wire tx_ready;
    wire [7:0] sca_tx_addr[DOWNLINKCOUNT-1:0];
    wire [7:0] sca_tx_ctrl[DOWNLINKCOUNT-1:0];
    wire [7:0] sca_tx_trid[DOWNLINKCOUNT-1:0];
    wire [7:0] sca_tx_ch[DOWNLINKCOUNT-1:0];
    wire [7:0] sca_tx_len[DOWNLINKCOUNT-1:0];
    wire [7:0] sca_tx_cmd[DOWNLINKCOUNT-1:0];
    wire [31:0] sca_tx_data[DOWNLINKCOUNT-1:0];



    wire [SCA_PER_DOWNLINK-1:0] sca_rx_done[DOWNLINKCOUNT-1:0];  //  Reply received flag (pulse)
    wire [7:0] rx_addr[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //  --! Reply: address field (According to the SCA manual)
    wire [7:0] rx_ctrl[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //  --! Reply: control field (According to the SCA manual)
    wire [7:0] rx_trid[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //  --! Reply: transaction ID field (According to the SCA manual)
    wire [7:0] rx_ch[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //    --! Reply: channel field (According to the SCA manual)
    wire [7:0] rx_len[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //   --! Reply: len field (According to the SCA manual)
    wire [7:0] rx_err[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //   --! Reply: error field (According to the SCA manual)
    wire [31:0] rx_data[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  // --!Reply: data field (According to the SCA manual)


    // -- EC line
    wire [1:0]sca_line_tx[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];       // --! (TX) Array of bits to be mapped to the TX GBT-Frame
    wire [1:0]sca_line_rx[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];       // --! (RX) Array of bits to be mapped to the RX GBT-Frame



    // -- IC Debug
    // ------------
    wire [DOWNLINKCOUNT-1:0] ic_ready;
    wire [DOWNLINKCOUNT-1:0] ic_empty;

    wire [7:0] GBTx_address[DOWNLINKCOUNT-1:0];
    wire [15:0] Register_ptr[DOWNLINKCOUNT-1:0];
    wire [15:0] ic_nb_to_be_read_rd[DOWNLINKCOUNT-1:0];

    wire [DOWNLINKCOUNT-1:0] ic_start_wr;
    wire [DOWNLINKCOUNT-1:0] ic_wr;
    wire [7:0] ic_data_wr[DOWNLINKCOUNT-1:0];  

    wire [DOWNLINKCOUNT-1:0] ic_start_rd;    
    wire [DOWNLINKCOUNT-1:0] ic_rd;
    wire [7:0] ic_data_rd[DOWNLINKCOUNT-1:0];

    wire [7:0] GBTx_rd_addr[DOWNLINKCOUNT-1:0];
    wire [15:0] GBTx_rd_mem_ptr[DOWNLINKCOUNT-1:0];
    wire [15:0] GBTx_rd_mem_size[DOWNLINKCOUNT-1:0];
    wire ic_empty_rd;

    //to gbtic
    wire [7:0] GBTx_address_to_gbtic[DOWNLINKCOUNT-1:0];
    wire [15:0] Register_addr_to_gbtic[DOWNLINKCOUNT-1:0];
    wire [15:0] nb_to_be_read_to_gbtic[DOWNLINKCOUNT-1:0];

    wire [DOWNLINKCOUNT-1:0] start_write_to_gbtic;
    wire [DOWNLINKCOUNT-1:0] wr_to_gbtic;
    wire [7:0] data_to_gbtic[DOWNLINKCOUNT-1:0];  

    wire [DOWNLINKCOUNT-1:0] start_read_to_gbtic; 
    wire [DOWNLINKCOUNT-1:0] rd_to_gbtic;
    wire [7:0] data_from_gbtic[DOWNLINKCOUNT-1:0];

    wire [7:0] ic_rd_gbtx_addr[DOWNLINKCOUNT-1:0];
    wire [15:0] ic_rd_mem_ptr[DOWNLINKCOUNT-1:0];
    wire [15:0] ic_rd_nb_of_words[DOWNLINKCOUNT-1:0];

    //elink sca data, only comes from master lpGBT
    wire [DOWNLINKCOUNT-1:0]txFrameClkPllLocked_from_gbtExmplDsgn;


    wire [1:0] down_elink_80m_sca_pri[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];
    wire [1:0] down_elink_80m_sca_aux[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];



    // wire [7:0] uplink_320m_sca_aux[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];
    wire [7:0] uplink_320m_sca_pri[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];



    // -- Frame clock:
    // ---------------
    wire txFrameClk_from_txPll;

    wire fast_clock_for_meas;
    wire [31:0] cmd_delay;

    wire [1:0] ttc_ctrl;
    wire trigger_SMA;

    wire LOCK_TOP;


    wire    [7:0]   GBTx_address_uart;
    wire    [15:0]  Register_addr_uart;
    wire    [7:0]   nb_to_be_read_uart;
    wire            ic_wr_start_uart;
    wire            ic_rd_start_uart;
    wire    [7:0]   ic_wfifo_data_uart;
    wire            ic_fifo_wr_uart;
    wire            sca_reset_gbtsc_uart;
    wire            sca_start_reset_uart;
    wire            sca_start_connect_uart;
    wire            sca_start_command_uart;
    wire    [2:0]   sca_enable_array_uart;
    wire    [7:0]   sca_tx_address_uart;
    wire    [7:0]   sca_tx_transID_uart;
    wire    [7:0]   sca_tx_channel_uart;
    wire    [7:0]   sca_tx_len_uart;
    wire    [7:0]   sca_tx_command_uart;
    wire    [31:0]  sca_tx_data_uart;
    
    //uart
    wire            command_fifo_read,command_fifo_empty;
    wire    [159:0] command;    
    wire            uart_data_write;
    wire            uart_data_write_sca;
    wire    [7:0]   uart_data;
    wire    [7:0]   uart_data_sca;
    wire            uart_tx_ready;


    //Ethernet
    wire sys_clk_160;
    wire clk_200;
    wire tx_axis_clk;
    wire [7:0] tx_axis_fifo_tdata;
    wire tx_axis_fifo_tvalid;
    wire tx_axis_fifo_tready;
    wire tx_axis_fifo_tlast;




    // -- Reset controll    
    assign SFP_TX_DISABLE = 'b0;

wire [7:0] uplink_320m [DOWNLINKCOUNT-1:0][27:0];


genvar i,k,m;
for (i = 0; i < DOWNLINKCOUNT; i++) begin : downlink
    for (k = 0; k < SCA_PER_DOWNLINK; k++) begin
        //2-bit downlink sca tx mapping
        assign down_elink_80m_sca_pri[i][k] = sca_line_tx[i][k];
        assign down_elink_80m_sca_aux[i][k] = 2'b0;

        //2-bit uplink sca rx mapping
        assign sca_line_rx[i][k] = {uplink_320m_sca_pri[i][k][4],uplink_320m_sca_pri[i][k][0]};
    end

    for (m = 0; m < 28; m++) begin
        assign uplink_320m[i][m] = lpgbtfpga_uplinkUserData[2*i][m*8+7:m*8]; //only look for master uplink data
    end
    assign uplink_320m_sca_pri[i][0] = uplink_320m[i][16];
    assign uplink_320m_sca_pri[i][1] = uplink_320m[i][11];
    assign uplink_320m_sca_pri[i][2] = uplink_320m[i][13];
    assign uplink_320m_sca_pri[i][3] = uplink_320m[i][15];
    
    // for 4 SCA version 
    assign lpgbtfpga_downlinkUserData[i] = {4'b0,down_elink_80m_sca_pri[i][0],2'b00,down_elink_80m_sca_aux[i][0],
                                                 down_elink_80m_sca_pri[i][3],2'b00,down_elink_80m_sca_aux[i][3],
                                                 down_elink_80m_sca_pri[i][2],down_elink_80m_sca_aux[i][2],
                                                 down_elink_80m_sca_aux[i][1],down_elink_80m_sca_pri[i][1],
                                                 4'b0000,ttc_ctrl,2'b00,ttc_ctrl};   
    // for 3 SCA version
    // assign lpgbtfpga_downlinkUserData[i] = {8'b0,down_elink_80m_sca_aux[i][0],2'b00,down_elink_80m_sca_pri[i][0],
    //                                             down_elink_80m_sca_pri[i][2],2'b00,down_elink_80m_sca_aux[i][2],
    //                                             down_elink_80m_sca_aux[i][1],down_elink_80m_sca_pri[i][1],8'b0,ttc_ctrl};
                                            

    assign lpgbtfpga_downlinkEcData[i] = 2'b11;
    assign sca_tx_addr[i]=sca_tx_address_uart;
    assign sca_tx_ctrl[i]='b0;
    assign sca_tx_trid[i]=sca_tx_transID_uart;
    assign sca_tx_ch[i]=sca_tx_channel_uart;
    assign sca_tx_len[i]=sca_tx_len_uart;
    assign sca_tx_cmd[i]=sca_tx_command_uart;
    assign sca_tx_data[i]=sca_tx_data_uart;

    assign sca_reset[i] = sca_reset_gbtsc_uart;
    assign sca_start_reset[i] = sca_start_reset_uart;
    assign sca_start_connect[i] = sca_start_connect_uart;
    assign sca_start[i] = sca_start_command_uart;
    assign sca_enable_array[i] = sca_enable_array_uart;


    //     // -- Data pattern generator / checker (PRBS7)
    // lpgbtfpga_patterngen lpgbtfpga_patterngen_inst(
    //     // --clk40Mhz_Tx_i      : in  std_logic;
    //     .clk320DnLink_i(lpgbtfpga_clk40),
    //     .clkEnDnLink_i(1'b1),
    //     .generator_rst_i(generator_rst[i]),

    //     // -- Group configurations:
    //     // --    "11": 320Mbps
    //     // --    "10": 160Mbps
    //     // --    "01": 80Mbps
    //     // --    "00": Fixed pattern
    //     .config_group0_i           (downconfig_g0[i]),
    //     .config_group1_i           (downconfig_g1[i]),
    //     .config_group2_i           (downconfig_g2[i]),
    //     .config_group3_i           (downconfig_g3[i]),
    //     .downlink_o                (lpgbtfpga_downlinkUserData_fromgen[i]),
    //     .fixed_pattern_i           ('h12345678),
    //     .eport_gen_rdy_o           (downlink_gen_rdy[i])
    // );

    vio_downlink vio_downlink_inst (
        .clk(mgt_freedrpclk),                  // input wire clk
        .probe_in0(lpgbtfpga_downlinkrdy[i]),      // input wire [0 : 0] probe_in0
        // .probe_in1(downlink_gen_rdy[i]),      // input wire [15 : 0] probe_in1
        .probe_in1(downlinkPhase[i]),      // input wire [9 : 0] probe_in1
        .probe_in2(lpgbtfpga_mgt_txaligned[i]),      // input wire [0 : 0] probe_in2
        .probe_in3(lpgbtfpga_mgt_txpiphase[i]),      // input wire [6 : 0] probe_in3
        

        .probe_out0(lpgbtfpga_downlinkrst[i]),    // output wire [0 : 0] probe_out0
        .probe_out1(downLinkBypassInterleaver[i]),    // output wire [0 : 0] probe_out1
        .probe_out2(downLinkBypassFECEncoder[i]),    // output wire [0 : 0] probe_out2
        .probe_out3(downLinkBypassScrambler[i]),    // output wire [0 : 0] probe_out3
        .probe_out4(lpgbtfpga_mgt_txpicalib[i]),    // output wire [6 : 0] probe_out4
        .probe_out5(lpgbtfpga_mgt_txcaliben[i]),    // output wire [0 : 0] probe_out5

        .probe_out6(lpgbtfpga_mgt_txpolarity[i]),  // output wire [0 : 0] probe_out6
        .probe_out7(downlinkPhaseCalib[i]),  // output wire [9 : 0] probe_out7
        .probe_out8(downlinkPhaseForce[i])  // output wire [0 : 0] probe_out8
        // .probe_out6(generator_rst[i]),    // output wire [0 : 0] probe_out6
        // .probe_out7(downconfig_g0[i]),    // output wire [1 : 0] probe_out7
        // .probe_out8(downconfig_g1[i]),    // output wire [1 : 0] probe_out8
        // .probe_out9(downconfig_g2[i]),    // output wire [1 : 0] probe_out9
        // .probe_out10(downconfig_g3[i]),  // output wire [1 : 0] probe_out10
        
    );


    gbtsc_top #(
        // -- IC configuration
        .g_ic_fifo_depth(453), //  --! Defines the depth of the FIFO used to handle the Internal control (Max. number of words/bytes can be read/write from/to a GBTx)
        .g_tolpgbt(1),    //      --! 1 to use LpGBT. Otherwise, it should be 0

        // -- EC configuration
        .g_sca_count(SCA_PER_DOWNLINK)//              --! Defines the maximum number of SCA that can be connected to this module
    )
    gbtsc_inst (
        // -- Clock & reset
        .tx_clk_i           (txFrameClk_from_txPll),       //                            --! Tx clock (Tx_frameclk_o from GBT-FPGA IP): must be a multiple of the LHC frequency
        .tx_clk_en          (1'b1),                        //    --! Tx clock enable signal must be used in case of multi-cycle path(tx_clk_i > LHC frequency). By default: always enabled

        .rx_clk_i           (txFrameClk_from_txPll),       //                             --! Rx clock (Rx_frameclk_o from GBT-FPGA IP): must be a multiple of the LHC frequency
        .rx_clk_en          (1'b1),                        //    --! Rx clock enable signal must be used in case of multi-cycle path(rx_clk_i > LHC frequency). By default: always enabled

        .rx_reset_i         (sca_reset[i]),   //                                   --! Reset RX datapath
        .tx_reset_i         (sca_reset[i]),   //                                  --! Reset TX datapath

        // -- IC control
        .tx_start_write_i    (ic_wr_start_uart),        //                            --! Request a write config. to the GBTx (IC)
        .tx_start_read_i     (ic_rd_start_uart),         //                          --! Request a read config. to the GBTx (IC)

        // -- IC configuration
        .tx_gbtx_address_i   (GBTx_address_uart),       //          --! I2C address of the GBTx
        .tx_register_addr_i  (Register_addr_uart),      //          --! Address of the first register to be accessed
        .tx_nb_to_be_read_i  (nb_to_be_read_uart),      //         --! Number of words/bytes to be read (only for read transactions)

        // -- IC FIFO control
        .wr_clk_i            (txFrameClk_from_txPll),  //             --! Fifo's writing clock
        .tx_wr_i             (ic_fifo_wr_uart),            //                      --! Request a write operation into the internal FIFO (Data to GBTx)
        .tx_data_to_gbtx_i   (ic_wfifo_data_uart),           //      --! Data to be written into the internal FIFO

        .rd_clk_i            (txFrameClk_from_txPll),    
        .rx_rd_i             (uart_tx_ready),              //                     --! Request a read operation of the internal FIFO (GBTx reply)
        .rx_data_from_gbtx_o (data_from_gbtic[i]),          //     --! Data from the FIFO

        // -- IC Status
        .tx_ready_o          (ic_ready[i]),                 //                  --! IC core ready for a transaction
        .rx_empty_o          (ic_empty[i]),                  //                --! Rx FIFO is empty (no reply from GBTx)

        .rx_gbtx_addr_o      (ic_rd_gbtx_addr[i]),         //        --! I2C address of the GBTx (read from a reply)
        .rx_mem_ptr_o        (ic_rd_mem_ptr[i]),           //    --! I2C address of the first register read/written
        .rx_nb_of_words_o    (ic_rd_nb_of_words[i]),       //        --! Number of words/bytes read/written

        // -- SCA control
        .sca_enable_i        (sca_enable_array[i]),   //--! Enable flag to select SCAs
        .start_reset_cmd_i   (sca_start_reset[i]),        //                           --! Send a reset command to the enabled SCAs
        .start_connect_cmd_i (sca_start_connect[i]),      //                            --! Send a connect command to the enabled SCAs
        .start_command_i     (sca_start[i]),          //                         --! Send the command set in input to the enabled SCAs
        .inject_crc_error    (1'b0),               //                   --! Emulate a CRC error

        // -- SCA command
        .tx_address_i        (sca_tx_addr[i]),           //     --! Command: address field (According to the SCA manual)
        .tx_transid_i        (sca_tx_trid[i]),           //    --! Command: transaction ID field (According to the SCA manual)
        .tx_channel_i        (sca_tx_ch[i]),             //   --! Command: channel field (According to the SCA manual)
        .tx_command_i        (sca_tx_cmd[i]),            //    --! Command: command field (According to the SCA manual)
        .tx_data_i           (sca_tx_data[i]),           //    --! Command: data field (According to the SCA manual)

        .rx_received_o    (sca_rx_done[i]),   //--! Reply received flag (pulse)
        .rx_address_o     (rx_addr[i]),       //   --! Reply: address field (According to the SCA manual)
        .rx_control_o     (open),          //--! Reply: control field (According to the SCA manual)
        .rx_transid_o     (rx_trid[i]),       //   --! Reply: transaction ID field (According to the SCA manual)
        .rx_channel_o     (rx_ch[i]),         // --! Reply: channel field (According to the SCA manual)
        .rx_len_o         (rx_len[i]),        // --! Reply: len field (According to the SCA manual)
        .rx_error_o       (rx_err[i]),        //   --! Reply: error field (According to the SCA manual)
        .rx_data_o        (rx_data[i]),       //   --! Reply: data field (According to the SCA manual)

        // -- EC line
        .sca_data_o        (sca_line_tx[i]), // -- downlink -- txData_to_gbtExmplDsgn(81 downto 80),        --! (TX) Array of bits to be mapped to the TX GBT-Frame
        .sca_data_i        (sca_line_rx[i]), //-- uplink  -- rxData_from_gbtExmplDsgn(81 downto 80),        --! (RX) Array of bits to be mapped to the RX GBT-Frame
        //ec_data_i goes to sca_deserializer


        // -- IC lines
        .ic_data_o           (lpgbtfpga_downlinkIcData[i]),   //             --! (TX) Array of bits to be mapped to the TX GBT-Frame (bits 83/84)
        .ic_data_i           (lpgbtfpga_uplinkIcData[i])     //            --! (RX) Array of bits to be mapped to the RX GBT-Frame (bits 83/84)

    );

end

sca_sendback # (
    .DOWNLINKCOUNT(DOWNLINKCOUNT),
    .SCA_PER_DOWNLINK(SCA_PER_DOWNLINK)) 
inst_sca_sendback
(
    .clk                 (txFrameClk_from_txPll),
    .rst                 (reset_upchecker),
    .rx_reply_received_i (sca_rx_done),
    .rx_address_i        (rx_addr),
    .rx_transID_i        (rx_trid),
    .rx_channel_i        (rx_ch),
    .rx_len_i            (rx_len),
    .rx_error_i          (rx_err),
    .rx_data_i           (rx_data),
    .uart_tx_ready_i     (uart_tx_ready),
    .uart_data_o         (uart_data_sca),
    .uart_data_write_o   (uart_data_write_sca)
);

command_resolve inst_command_resolve
(
    .clk                  (txFrameClk_from_txPll),
    .rst                  (reset_upchecker),
    .command_i            (command),
    .command_fifo_empty_i (command_fifo_empty),
    .command_fifo_read_o  (command_fifo_read),
    // .data_back_o          (data_back_uart),
    // .data_back_wr_o       (data_back_wr_uart),
    .GBTx_address_o       (GBTx_address_uart),
    .Register_addr_o      (Register_addr_uart),
    .nb_to_be_read_o      (nb_to_be_read_uart[7:0]),
    .ic_wr_start_o        (ic_wr_start_uart),
    .ic_rd_start_o        (ic_rd_start_uart),
    .ic_wfifo_data_o      (ic_wfifo_data_uart),
    .ic_fifo_wr_o         (ic_fifo_wr_uart),

    .reset_gbtsc_o        (sca_reset_gbtsc_uart),
    .start_reset_o        (sca_start_reset_uart),
    .start_connect_o      (sca_start_connect_uart),
    .start_command_o      (sca_start_command_uart),
    .sca_enable_array_o   (sca_enable_array_uart),
    
    .tx_address_o         (sca_tx_address_uart),
    .tx_transID_o         (sca_tx_transID_uart),
    .tx_channel_o         (sca_tx_channel_uart),
    .tx_len_o             (sca_tx_len_uart),
    .tx_command_o         (sca_tx_command_uart),
    .tx_data_o            (sca_tx_data_uart)
);

assign uart_data = ic_empty[0] ? uart_data_sca : data_from_gbtic[0];
assign uart_data_write = ic_empty[0] ? uart_data_write_sca : (~ic_empty[0]);

UART_interface UART_interface_inst
(
    .clk40                (txFrameClk_from_txPll),
    .reset                (reset_upchecker),
    .rxd_i                (uart_rxd),
    .txd_o                (uart_txd),
    .command_fifo_rd_en_i (command_fifo_read),
    .command_o            (command),
    .command_fifo_empty_o (command_fifo_empty),

    .tx_start_i           (uart_data_write),
    .tx_data_i            (uart_data),
    .tx_ready_o           (uart_tx_ready)

);

genvar j;
for (j = 0; j < UPLINKCOUNT; j++) begin : uplink
    vio_uplink vio_uplink_inst (
        .clk(mgt_freedrpclk),                // input wire clk
        .probe_in0(lpgbtfpga_uplinkrdy[j]),    // input wire [0 : 0] probe_in0
        .probe_in1(uperror_detected[j]),    // input wire [27 : 0] probe_in1
        .probe_in2(upLinkFECCorrectedLatched[j]),    // input wire [0 : 0] probe_in2
        .probe_in3(uplinkPhase[j]),    // input wire [2 : 0] probe_in3

        .probe_out0(lpgbtfpga_uplinkrst[j]),  // output wire [0 : 0] probe_out0
        .probe_out1(upLinkInterleaverBypass[j]),  // output wire [0 : 0] probe_out1
        .probe_out2(upLinkFecBypass[j]),  // output wire [0 : 0] probe_out2
        .probe_out3(upLinkScramblerBypass[j]),  // output wire [0 : 0] probe_out3
        .probe_out4(reset_upchecker[j]),  // output wire [0 : 0] probe_out4
        .probe_out5(upelink_config[j]),  // output wire [53 : 0] probe_out5
        .probe_out6(lpgbtfpga_mgt_rxpolarity[j]),  // output wire [0 : 0] probe_out6
        .probe_out7(upLinkFECCorrectedClear[j]),  // output wire [0 : 0] probe_out7
        .probe_out8(uplinkPhaseCalib[j]),  // output wire [2 : 0] probe_out8
        .probe_out9(uplinkPhaseForce[j])  // output wire [0 : 0] probe_out9
);
end



// -- Clocks
    
// -- MGT(GTX) reference clock:
// ----------------------------   
// -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
// --          * The MGT reference clock frequency must be 320MHz for the latency-optimized GBT Bank. 

// IBUFDS_GTE3: Gigabit Transceiver Buffer
//              UltraScale
// Xilinx HDL Language Template, version 2021.1

IBUFDS_GTE3 #(
   .REFCLK_EN_TX_PATH(1'b0),   // Refer to Transceiver User Guide.
   .REFCLK_HROW_CK_SEL(2'b00), // Refer to Transceiver User Guide.
   .REFCLK_ICNTL_RX(2'b00)     // Refer to Transceiver User Guide.
)
smaMgtRefClkIbufdsGtxe2 (
   .O(mgtRefClk_from_smaMgtRefClkbuf),         // 1-bit output: Refer to Transceiver User Guide.
   .ODIV2(), // 1-bit output: Refer to Transceiver User Guide.
   .CEB(1'b0),     // 1-bit input: Refer to Transceiver User Guide.
   .I(SMA_MGT_REFCLK_P),         // 1-bit input: Refer to Transceiver User Guide.
   .IB(SMA_MGT_REFCLK_N)        // 1-bit input: Refer to Transceiver User Guide.
);

// -- Frame clock

xlx_k7v7_tx_pll txPll
(
    .txFrameClk  (txFrameClk_from_txPll),
    .sys_clk_160 (sys_clk_160),
    .reset       (1'b0),
    .locked      (txFrameClkPllLocked_from_gbtExmplDsgn),
    .clk_in1     (lpgbtfpga_clk40)
);
assign LOCK_TOP = txFrameClkPllLocked_from_gbtExmplDsgn;

IBUFDS 
IBUFDS_inst (
   .O(trigger_SMA),   // 1-bit output: Buffer output
   .I(TRIGGER0_P),   // 1-bit input: Diff_p buffer input (connect directly to top-level port)
   .IB(TRIGGER0_N)  // 1-bit input: Diff_n buffer input (connect directly to top-level port)
);
    
sys_clk sys_clk_inst
(
    .mgt_freedrpclk (mgt_freedrpclk),
    .clk_200        (clk_200),
    .locked         (locked),
    .clk_in1_p      (USER_CLOCK_P),
    .clk_in1_n      (USER_CLOCK_N)
);

            
  // -- In this example design, the 40MHz clock used for the user logic is derived from a division of the Tx user clock of the MGT
  // -- It should be noted, that in realistic cases, this clock typically comes from an external PLL (sync. to the MGT Tx reference clock)
  
BUFGCE_DIV #(
   .BUFGCE_DIVIDE(8),              // 1-8
   // Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
   .IS_CE_INVERTED(1'b0),          // Optional inversion for CE
   .IS_CLR_INVERTED(1'b0),         // Optional inversion for CLR
   .IS_I_INVERTED(1'b0),           // Optional inversion for I
   .SIM_DEVICE("ULTRASCALE"))  // ULTRASCALE, ULTRASCALE_PLUS
BUFGCE_DIV_inst (
   .O(lpgbtfpga_clk40),     // 1-bit output: Buffer
   .CE(1'b1),   // 1-bit input: Buffer enable
   .CLR(1'b0), // 1-bit input: Asynchronous clear
   .I(lpgbtfpga_mgttxclk)      // 1-bit input: Buffer 320M clk_mgtRxClk_o
);

lpgbtFpga_10g24 #(
    .FEC(FEC5),
    .UPLINKCOUNT(UPLINKCOUNT),
    .DOWNLINKCOUNT(DOWNLINKCOUNT))
lpgbtFpga_top_inst (
//Clocks
    .downlinkClk_i               (lpgbtfpga_clk40),
    .uplinkClk_i                 (lpgbtfpga_clk40),
    .downlinkRst_i               (lpgbtfpga_downlinkrst),
    .uplinkRst_i                 (lpgbtfpga_uplinkrst),
//Downlink
    .downlinkUserData_i          (lpgbtfpga_downlinkUserData),
    .downlinkEcData_i            (lpgbtfpga_downlinkEcData),
    .downlinkIcData_i            (lpgbtfpga_downlinkIcData),
    .downLinkBypassInterleaver_i (downLinkBypassInterleaver),
    .downLinkBypassFECEncoder_i  (downLinkBypassFECEncoder),
    .downLinkBypassScrambler_i   (downLinkBypassScrambler),
    .downlinkReady_o             (lpgbtfpga_downlinkrdy),
    .downlinkPhase_o             (downlinkPhase),
    .downlinkPhaseCalib_i        (downlinkPhaseCalib),
    .downlinkPhaseForce_i        (downlinkPhaseForce),
//uplink
    .uplinkUserData_o            (lpgbtfpga_uplinkUserData),
    .uplinkEcData_o              (lpgbtfpga_uplinkEcData),
    .uplinkIcData_o              (lpgbtfpga_uplinkIcData),
    .uplinkBypassInterleaver_i   (upLinkInterleaverBypass),
    .uplinkBypassFECEncoder_i    (upLinkFecBypass),
    .uplinkBypassScrambler_i     (upLinkScramblerBypass),
    .uplinkFECCorrectedClear_i   (upLinkFECCorrectedClear),
    .uplinkFECCorrectedLatched_o (upLinkFECCorrectedLatched),
    .uplinkReady_o               (lpgbtfpga_uplinkrdy),
    .uplinkPhase_o               (uplinkPhase),
    .uplinkPhaseCalib_i          (uplinkPhaseCalib),
    .uplinkPhaseForce_i          (uplinkPhaseForce),
//MGT
    .mgt_rxn_i                   (SFP_RX_N),
    .mgt_rxp_i                   (SFP_RX_P),
    .mgt_txn_o                   (SFP_TX_N),
    .mgt_txp_o                   (SFP_TX_P),
    .clk_mgtrefclk_i             (mgtRefClk_from_smaMgtRefClkbuf),
    .clk_mgtfreedrpclk_i         (mgt_freedrpclk),
    .clk_mgtRxClk_o              (lpgbtfpga_mgtrxclk),
    .clk_mgtTxClk_o              (lpgbtfpga_mgttxclk),
    .mgt_txpolarity_i            (lpgbtfpga_mgt_txpolarity),
    .mgt_rxpolarity_i            (lpgbtfpga_mgt_rxpolarity),
    .mgt_txcaliben_i             (lpgbtfpga_mgt_txcaliben),
    .mgt_txcalib_i               (lpgbtfpga_mgt_txpicalib),
    .mgt_txaligned_o             (lpgbtfpga_mgt_txaligned),
    .mgt_txphase_o               (lpgbtfpga_mgt_txpiphase)
);

 

// --    lpgbtfpga_patternchecker_inst: lpgbtfpga_patternchecker
// --        port map(
// --            reset_checker_i  => reset_upchecker_s,
// --            ser320_clk_i     => lpgbtfpga_clk40,
// --            ser320_clkEn_i   => '1',
    
// --            data_rate_i      => uplinkSelectDataRate_s,
    
// --            elink_config_i   => upelink_config_s,
    
// --            error_detected_o => uperror_detected_s,
    
// --            userDataUpLink_i => lpgbtfpga_uplinkUserData_s
// --        );




// tdc_decoder_top# (
//     .UPLINKCOUNT(UPLINKCOUNT))
// tdc_decoder_top_inst
// (
//     .clk_40              (lpgbtfpga_clk40),
//     .rst_in              (reset_upchecker),
//     .userDataUpLink      (lpgbtfpga_uplinkUserData),
//     .trigger_in          (trigger_SMA),
//     // .error_detected_o    (error_detected_o),
//     .encode_ttc          (ttc_ctrl),
//     .hit                 (hit),
//     .mezz_TRST           (mezz_TRST),
//     .sys_clk_160         (sys_clk_160),
//     .tx_axis_clk         (tx_axis_clk),
//     .tx_axis_fifo_tdata  (tx_axis_fifo_tdata),
//     .tx_axis_fifo_tvalid (tx_axis_fifo_tvalid),
//     .tx_axis_fifo_tready (tx_axis_fifo_tready),
//     .tx_axis_fifo_tlast  (tx_axis_fifo_tlast)
// );


assign USER_SMA_GPIO_P =1'b0;
assign USER_SMA_GPIO_N =1'b0;
  

assign ttc_ctrl          = 'b0;
assign hit                 = 'b0;
assign mezz_TRST           = 'b0;
assign tx_axis_fifo_tdata  = 'b0;
assign tx_axis_fifo_tvalid = 'b0;
assign tx_axis_fifo_tlast  = 'b0;    


BUFG delaymeasclk_inst (
   .O(fast_clock_for_meas), // 1-bit output: Clock output.
   .I(mgtRefClk_from_smaMgtRefClkbuf)  // 1-bit input: Clock input.
);



tri_mode_ethernet_mac_0_example_design ethernet_mac
(
    .glbl_rst            (reset_upchecker),
    .clk_200             (clk_200),

    //-- 125 MHz clock from MMCM
    .gtx_clk_bufg_out    (tx_axis_clk),
    .phy_resetn          (phy_resetn),

    //-- RGMII terface
    .rgmii_txd           (rgmii_txd),
    .rgmii_tx_ctl        (rgmii_tx_ctl),
    .rgmii_txc           (rgmii_txc),
    .rgmii_rxd           (rgmii_rxd),
    .rgmii_rx_ctl        (rgmii_rx_ctl),
    .rgmii_rxc           (rgmii_rxc),

    //-- MDIO terface
    .mdio                (mdio),
    .mdc                 (mdc),

    //-- USER side TX AXI-S terface       
    .tx_axis_fifo_tdata  (tx_axis_fifo_tdata),
    .tx_axis_fifo_tvalid (tx_axis_fifo_tvalid),
    .tx_axis_fifo_tready (tx_axis_fifo_tready),
    .tx_axis_fifo_tlast  (tx_axis_fifo_tlast)
);


endmodule
