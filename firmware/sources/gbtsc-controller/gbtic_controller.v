/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : gbtic_controller.v
// Create : 2023-09-29 16:45:39
// Revise : 2023-10-10 19:37:10
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------



module gbtic_controller
(
    input clk,
    input rst,

    input ic_ready_i,
    input ic_empty_i,
    
    //configuration
    // output [7:0] GBTx_address_o,
    // output [15:0] Register_addr_o,
    // output [15:0] nb_to_be_read_o,

    // //control
    // output ic_wr_start_o,
    // output ic_rd_start_o,

    // //write to internal ic FIFO
    // output [7:0] ic_wfifo_data_o,
    // output ic_fifo_wr_o,

    //read from internal ic FIFO
    // input [7:0] ic_rfifo_data_i,
    // output ic_fifo_rd_o,

    // //uart interface
    // input  rxd_i,
    // output txd_o,
    
    // //To GBT-SC
    // output reset_gbtsc_o,
    // output start_reset_o,
    // output start_connect_o,
    // output start_command_o,
    // output [2:0] sca_enable_array_o,
    
    // // input tx_ready_i,
    // output [7:0] tx_address_o,
    // output [7:0] tx_transID_o,
    // output [7:0] tx_channel_o,
    // output [7:0] tx_len_o,
    // output [7:0] tx_command_o,
    // output [31:0] tx_data_o,

    input [2:0] rx_reply_received_i,
    input [23:0] rx_address_i,
    input [23:0] rx_transID_i,
    input [23:0] rx_channel_i,
    input [23:0] rx_len_i,
    input [23:0] rx_error_i,
    input [95:0] rx_data_i
);




wire command_fifo_read,command_fifo_empty,data_back_wr;
wire [159:0] command,data_back;
wire uart_tx_ready;

wire [7:0]uart_data;
wire [7:0]uart_data_sca;

wire uart_data_write;
wire uart_data_write_sca;

assign uart_data_write = ic_empty_i ? uart_data_write_sca : (~ic_empty_i);
assign uart_data = ic_empty_i ? uart_data_sca : ic_rfifo_data_i;


assign nb_to_be_read_o [15:8] = 'b0;
assign ic_fifo_rd_o = uart_tx_ready;










// vio_gbtic_controller vio_gbtic_controller_inst (
//     .clk(clk),                // input wire clk

//     .probe_in0(ic_ready),    // input wire [0 : 0] probe_in0
//     .probe_in1(ic_empty_i),    // input wire [0 : 0] probe_in1
//     .probe_in2(data_i),    // input wire [7 : 0] probe_in2
//     .probe_in3(rd_gbtx_addr),    // input wire [7 : 0] probe_in3
//     .probe_in4(rd_mem_ptr),    // input wire [15 : 0] probe_in4
//     .probe_in5(rd_nb_of_words),    // input wire [15 : 0] probe_in5

//     .probe_out0(GBTx_address),  // output wire [7 : 0] probe_out0
//     .probe_out1(Register_addr),  // output wire [15 : 0] probe_out1
//     .probe_out2(nb_to_be_read),  // output wire [15 : 0] probe_out2
//     .probe_out3(write_ic_vio),  // output wire [0 : 0] probe_out3
//     .probe_out4(read_ic_vio),  // output wire [0 : 0] probe_out4
//     .probe_out5(data_o),  // output wire [7 : 0] probe_out5
//     .probe_out6(write_fifo_vio),  // output wire [0 : 0] probe_out6
//     .probe_out7(read_fifo_vio)  // output wire [0 : 0] probe_out7
// );


// ila_gbtic_controller ila_gbtic_controller_inst (
//     .clk(clk), // input wire clk

//     .probe0(rxd), // input wire [0:0]  probe0  
//     .probe1(txd), // input wire [0:0]  probe1 
//     .probe2(command_fifo_read), // input wire [0:0]  probe2 
//     .probe3(command), // input wire [159:0]  probe3 
//     .probe4(command_fifo_empty), // input wire [0:0]  probe4 
//     .probe5(GBTx_address), // input wire [7:0]  probe5 
//     .probe6(Register_addr), // input wire [15:0]  probe6 
//     .probe7(nb_to_be_read[7:0]), // input wire [7:0]  probe7 
//     .probe8(ic_wr_start), // input wire [0:0]  probe8 
//     .probe9(ic_rd_start), // input wire [0:0]  probe9 
//     .probe10(ic_wfifo_data), // input wire [7:0]  probe10 
//     .probe11(ic_fifo_wr), // input wire [0:0]  probe11 
//     .probe12(ic_rfifo_data), // input wire [7:0]  probe12 
//     .probe13(ic_fifo_rd) // input wire [0:0]  probe13
// );


endmodule
