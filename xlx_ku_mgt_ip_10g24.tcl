##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2019.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source xlx_ku_mgt_ip_10g24.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./MiniDAQ_xcku035_triggerless/MiniDAQ_xcku035_triggerless.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project MiniDAQ_xcku035_triggerless MiniDAQ_xcku035_triggerless -part xcku035-fbva676-1-c
  set_property target_language Verilog [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:gtwizard_ultrascale:1.7 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP xlx_ku_mgt_ip_10g24
##################################################################

set gtwizard_ultrascale xlx_ku_mgt_ip_10g24
create_ip -name gtwizard_ultrascale -vendor xilinx.com -library ip -version 1.7 -module_name $gtwizard_ultrascale

set_property -dict { 
  CONFIG.CHANNEL_ENABLE {X0Y5 X0Y4}
  CONFIG.TX_MASTER_CHANNEL {X0Y4}
  CONFIG.RX_MASTER_CHANNEL {X0Y4}
  CONFIG.TX_LINE_RATE {10.24}
  CONFIG.TX_PLL_TYPE {QPLL0}
  CONFIG.TX_REFCLK_FREQUENCY {320}
  CONFIG.TX_BUFFER_MODE {1}
  CONFIG.TX_OUTCLK_SOURCE {TXPLLREFCLK_DIV1}
  CONFIG.RX_LINE_RATE {10.24}
  CONFIG.RX_PLL_TYPE {QPLL0}
  CONFIG.RX_REFCLK_FREQUENCY {320}
  CONFIG.RX_BUFFER_MODE {0}
  CONFIG.RX_JTOL_FC {6.1427714}
  CONFIG.SIM_CPLL_CAL_BYPASS {0}
  CONFIG.RX_COMMA_P_ENABLE {true}
  CONFIG.RX_COMMA_MASK {1111111111}
  CONFIG.RX_COMMA_ALIGN_WORD {4}
  CONFIG.RX_SLIDE_MODE {PCS}
  CONFIG.ENABLE_OPTIONAL_PORTS {drpaddr_in drpclk_in drpdi_in drpen_in drpwe_in rxpolarity_in txpippmen_in txpippmovrden_in txpippmpd_in txpippmsel_in txpippmstepsize_in txpolarity_in drpdo_out drprdy_out txbufstatus_out}
  CONFIG.RX_REFCLK_SOURCE {}
  CONFIG.TX_REFCLK_SOURCE {}
  CONFIG.TXPROGDIV_FREQ_SOURCE {QPLL0}
  CONFIG.TXPROGDIV_FREQ_VAL {320}
  CONFIG.FREERUN_FREQUENCY {200}
} [get_ips $gtwizard_ultrascale]

##################################################################

